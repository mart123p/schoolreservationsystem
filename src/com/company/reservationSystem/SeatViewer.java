package com.company.reservationSystem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SeatViewer extends JDialog {
    private int seatSelected;

    public SeatViewer() {
        setLayout(new BorderLayout());
        add(new PlaneCanvas(), BorderLayout.CENTER);
        add(new JLabel("Rouge = occupé"), BorderLayout.EAST);
        constructor();
    }
    public SeatViewer(int SeatSelected,Component root){
        setLayout(new BorderLayout());
        this.seatSelected = SeatSelected;
        ZaniDialog.MsgBox(root, "Le siège " + seatSelected + " vous a été reservé", "Félicitations");
        add(new PlaneCanvas(seatSelected), BorderLayout.CENTER);
        add(new JLabel("Vous avez le siège :" + seatSelected),BorderLayout.EAST);
        constructor();
    }
    private void constructor(){
        setModal(true);
        setPreferredSize(new Dimension(300, 580));

        JButton quit = new JButton("Retour au menu");
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(quit,BorderLayout.SOUTH);

        pack();
        setVisible(true);
    }
}
