package com.company.reservationSystem;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ReservationSystem extends JFrame {
    private JPanel contentPane;
    private JButton fumeur;
    private JButton nonFumeur;
    private JButton showSeats;
    private JButton resetButton;
    private JButton quitButton;

    public ReservationSystem() {
        setTitle("Système de Réservation");
        setContentPane(contentPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                System.exit(0);

            }
        });

        fumeur.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Seat.getSmokingSeatLeft() == 0){
                    if(Seat.getNonSmokingSeatLeft() != 0){
                        if(ZaniDialog.YesNo(getRootPane(),"Il n'y a pu de siège dans la section fumeur mais il \n en reste " + Seat.getNonSmokingSeatLeft() + " dans la section non fumeur. Désirez-vous en choisir un dans cette section?","Erreur")){
                            //We continue but as a non smoker
                            new SeatViewer(Seat.receiveNonSmokingSeat(),getRootPane());
                        }else{
                            ZaniDialog.MsgBox(getRootPane(),"Le départ du prochain vol est dans 4 heures","Avertissement");
                        }
                    }else{
                        ZaniDialog.MsgBox(getRootPane(),"Il n'y a pu de siège le système a atteint sa capacité maximale","Erreur");
                    }
                }else{
                    //We continue as a smoker
                    new SeatViewer(Seat.receiveSmokingSeat(),getRootPane());
                }

            }
        });

        nonFumeur.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Seat.getNonSmokingSeatLeft() == 0){
                    if(Seat.getSmokingSeatLeft() !=0){
                        if(ZaniDialog.YesNo(getRootPane(),"Il n'y a pu de siège dans la section non fumeur mais il \n en reste "+ Seat.getSmokingSeatLeft()+" dans la section fumeur. Désirez-vous en choisir un dans cette section?","Erreur")){
                            //Yes we can continue but has a smoker
                            new SeatViewer(Seat.receiveSmokingSeat(),getRootPane());
                        }else{
                            ZaniDialog.MsgBox(getRootPane(),"Le départ du prochain vol est dans 4 heures","Avertissement");
                        }
                    }else{
                        ZaniDialog.MsgBox(getRootPane(),"Il n'y a pu de siège le système a atteint sa capacité maximale","Erreur");
                    }
                }else{
                    //We continue normally
                    new SeatViewer(Seat.receiveNonSmokingSeat(),getRootPane());

                }
            }
        });

        showSeats.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SeatViewer();
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ZaniDialog.YesNo(getRootPane(),"Êtes-vous certains de vouloir remettre le système à zéro?","Avertissement")){
                    Seat.resetSeats();
                    ZaniDialog.MsgBox(getRootPane(),"Le système a été remis à zéro","Information");
                }
            }
        });

        pack();
        setVisible(true);

    }




    public static void main(String[] args) {
        ReservationSystem dialog = new ReservationSystem();
    }
}
