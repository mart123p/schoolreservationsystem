package com.company.reservationSystem;

import javax.swing.*;
import java.awt.*;

/**
 * Created by martinpouliot on 2015-08-25.
 */
public class PlaneCanvas extends JPanel {
    private boolean shownSelection = false;
    private int seatSelected = 0;
    public PlaneCanvas(int seatSelected){
        shownSelection = true;
        this.seatSelected = seatSelected -1;
    }
    public PlaneCanvas(){

    }

    public void paintComponent(Graphics g) {

        ImageIcon plane = new ImageIcon(getClass().getResource("/resources/plane.png"));
        ImageIcon seat = new ImageIcon(getClass().getResource("/resources/seat.png"));
        ImageIcon seatTaken = new ImageIcon(getClass().getResource("/resources/seatTaken.png"));
        ImageIcon seatSelected = new ImageIcon(getClass().getResource("/resources/seatSelected.png"));
        g.drawImage(plane.getImage(), 0, 0, null);
        int initialX = 62;
        int initialY = 140;
        boolean[] seats = Seat.getSeat();
        if(shownSelection){
                //We are in the smoking zone
                for(int i = 0; i < seats.length-5; i++){
                    if(i == this.seatSelected){
                        g.drawImage(seatSelected.getImage(),62,initialY,null);
                    }else if(seats[i]){
                        g.drawImage(seatTaken.getImage(),62,initialY,null);
                    }else{
                        g.drawImage(seat.getImage(),62,initialY,null);
                    }
                    initialY += 40;

                }
                initialY = 140;
                for(int i = 5; i < seats.length; i++){
                    if(i == this.seatSelected){
                        g.drawImage(seatSelected.getImage(),82,initialY,null);
                    }else if(seats[i]){
                        g.drawImage(seatTaken.getImage(),82,initialY,null);
                    }else{
                        g.drawImage(seat.getImage(),82,initialY,null);
                    }
                    initialY += 40;

                }

        }else{
            //Only display the seats

            for(int i = 0; i < seats.length-5; i++){
                if(seats[i]){
                    g.drawImage(seatTaken.getImage(),62,initialY,null);
                }else{
                    g.drawImage(seat.getImage(),62,initialY,null);
                }
                initialY += 40;

            }
            initialY = 140;
            for(int i = 5; i < seats.length; i++){
                if(seats[i]){
                    g.drawImage(seatTaken.getImage(),82,initialY,null);
                }else{
                    g.drawImage(seat.getImage(),82,initialY,null);
                }
                initialY += 40;

            }

        }

    }

}
