package com.company.reservationSystem;

import javax.swing.*;
import java.awt.*;

/**
 * Created by martinpouliot on 2015-07-30.
 */
public class ZaniDialog {

    /**
     *This class creates basic dialogs boxes faster than
     *the internal command
     *
     */
    public static boolean YesNo(String theMessage) {
        int result = JOptionPane.showConfirmDialog((Component) null, theMessage, "alert", JOptionPane.YES_NO_OPTION);
        return (result == 0) ? true:false;
    }
    public static boolean YesNo(String theMessage,String title) {
        int result = JOptionPane.showConfirmDialog((Component) null, theMessage,title, JOptionPane.YES_NO_OPTION);
        return (result == 0) ? true:false;
    }
    public static boolean YesNo(Component component,String theMessage,String title) {
        int result = JOptionPane.showConfirmDialog(component, theMessage,title, JOptionPane.YES_NO_OPTION);
        return (result == 0) ? true:false;
    }
    public static boolean YesNo(Component component,String theMessage) {
        int result = JOptionPane.showConfirmDialog(component, theMessage,"alert", JOptionPane.YES_NO_OPTION);
        return (result == 0) ? true:false;
    }
    public static int MsgBox(String theMessage, String title){
        int result = JOptionPane.showConfirmDialog((Component) null,theMessage,title, JOptionPane.PLAIN_MESSAGE);
        return result;
    }
    public static int MsgBox(Component component, String theMessage, String title){
        int result = JOptionPane.showConfirmDialog(component,theMessage,title, JOptionPane.PLAIN_MESSAGE);
        return result;
    }
    public static String InputBox(Component component,String theMessage, String textValue){
        String result = JOptionPane.showInputDialog(component,theMessage,textValue);
        return result;
    }
}
