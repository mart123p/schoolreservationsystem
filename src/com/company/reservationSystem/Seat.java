package com.company.reservationSystem;

/**
 * Created by martinpouliot on 2015-08-25.
 */
public class Seat {
    private static boolean[] seat = new boolean[10];

    public static int getSmokingSeatLeft(){
        int seatLeft = 0;
        for(int i = 0; i < (seat.length-5); i++){
            if(!seat[i]) {
                seatLeft++;
            }
        }
        return seatLeft;
    }


    public static int getNonSmokingSeatLeft(){
        int seatLeft = 0;
        for(int i = 5; i < seat.length; i++){
            if(!seat[i]){
                seatLeft++;
            }
        }
        return  seatLeft;
    }

    public static int receiveNonSmokingSeat(){
        int seatPosition = 0;
        boolean success = false;
        for(int i = 5; i < seat.length; i++){
            if(seat[i] == false){
                success = true;
                seat[i] = true;
                seatPosition = i;
                break;
            }
        }
        return seatPosition + 1;
    }

    public static int receiveSmokingSeat(){
        int seatPosition = 0;
        boolean success = false;
        for(int i = 0; i < seat.length-5; i++){
            if(seat[i] == false){
                success = true;
                seat[i] = true;
                seatPosition = i;
                break;
            }
        }
        return seatPosition + 1;
    }
    public static void resetSeats(){
        seat = new boolean[10];
    }
    public static boolean[] getSeat(){
        return seat;
    }
}
